const PORT = process.env.PORT || 3000;
const INDEX = '/index.html';
const express=require('express')
const server = express()
  .use((req, res) => res.sendFile(INDEX, { root: __dirname }),express.static( __dirname+ '/index.html'))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));
const io = require('socket.io')(server);
// server.use(express.static( __dirname+ '/index.html'))
io.on('connection', (socket) => {
  console.log('Client connected'+socket.id);
  socket.on('disconnect', () => console.log('Client disconnected'+socket.id));
});
